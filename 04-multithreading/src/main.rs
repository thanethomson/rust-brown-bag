extern crate clap;
extern crate rand;

use clap::{App, Arg};
use rand::seq;
use std::process;
use std::sync::mpsc;
use std::thread;

fn generate_random_numbers(count: u32) -> Vec<u32> {
    seq::sample_iter(&mut rand::thread_rng(), 1..10000, count as usize).unwrap()
}

fn reduce(numbers: &Vec<u32>) -> u32 {
    numbers.iter().fold(0, |a, b| a + b)
}

fn random_map_reduce(thread_count: u32, rnd_count: u32) -> u32 {
    let numbers = generate_random_numbers(rnd_count);
    println!("The original list of random numbers: {:?}", numbers);
    println!("The original list's sum: {}", reduce(&numbers));

    // let's map out the numbers to slices we can pass to each thread
    let per_thread = rnd_count / thread_count;
    let mut cur_thread_numbers: Vec<u32> = Vec::new();
    let mut thread_numbers: Vec<Vec<u32>> = Vec::new();

    for num in numbers.iter() {
        cur_thread_numbers.push(*num);
        if cur_thread_numbers.len() >= per_thread as usize {
            thread_numbers.push(cur_thread_numbers.clone());
            cur_thread_numbers.clear();
        }
    }

    // multiple producer, single consumer channel
    let (tx, rx) = mpsc::channel::<u32>();

    // now reduce, but multi-threaded
    for t in 0..thread_count {
        let tn = thread_numbers[t as usize].clone();
        let tx1 = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            let sum = reduce(&tn);
            println!("Thread {}, got {} values of sum {}", t, tn.len(), sum);
            tx1.send(sum).unwrap();
        });
    }

    let mut reduced: u32 = 0;
    for _ in 0..thread_count {
        let tsum = rx.recv().unwrap();
        reduced += tsum;
        println!("Parent thread got sum: {}", tsum);
    }

    reduced
}

fn main() {
    let matches = App::new("Simple multi-threading example")
        .arg(
            Arg::with_name("threads")
                .short("t")
                .long("threads")
                .takes_value(true)
                .default_value("5")
                .help("The number of threads to spawn")
        )
        .arg(
            Arg::with_name("count")
                .short("c")
                .long("count")
                .takes_value(true)
                .default_value("100")
                .help("The number of random numbers to generate to split amongst the threads")
        )
        .get_matches();
    
    let thread_count: u32 = match matches.value_of("threads").unwrap().parse() {
        Ok(tc) => tc,
        Err(_) => {
            println!("Error: please supply a valid thread count (as a positive integer)");
            process::exit(1);
        }
    };

    let rnd_count: u32 = match matches.value_of("count").unwrap().parse() {
        Ok(tc) => tc,
        Err(_) => {
            println!("Error: please supply a valid random number count (as a positive integer)");
            process::exit(2);
        }
    };

    let sum = random_map_reduce(thread_count, rnd_count);
    println!("Map/reduce sum: {}", sum);
}
