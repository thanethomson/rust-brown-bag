CREATE TABLE users (
    id            SERIAL PRIMARY KEY,
    email         VARCHAR(500) NOT NULL UNIQUE,
    first_name    VARCHAR(300),
    last_name     VARCHAR(300),
    password_hash VARCHAR(100)
);