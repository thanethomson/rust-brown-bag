use diesel::result::Error;
use rocket::http::Status;
use rocket::response::Failure;

#[derive(Serialize, Deserialize)]
pub struct Response {
    pub message: String
}

pub fn error_status(error: Error) -> Failure {
    Failure(
        match error {
            Error::NotFound => Status::NotFound,
            _               => Status::InternalServerError
        }
    )
}
