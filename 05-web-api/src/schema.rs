table! {
    users (id) {
        id -> Int4,
        email -> Varchar,
        first_name -> Nullable<Varchar>,
        last_name -> Nullable<Varchar>,
        password_hash -> Nullable<Varchar>,
    }
}
