use schema::users;

#[derive(Identifiable, Queryable, AsChangeset, Debug)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub email: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password_hash: Option<String>
}

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct InsertableUser {
    pub email: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password_hash: Option<String>
}
