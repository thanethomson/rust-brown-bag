#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
extern crate serde;

mod db;
mod common;
mod models;
mod schema;
mod users;

use rocket::Rocket;
use rocket_contrib::Json;

use self::db::*;
use self::common::*;
use self::users::*;

#[get("/")]
fn index() -> &'static str {
    "Hello world!"
}

#[catch(404)]
fn not_found() -> Json<Response> {
    Json(
        Response {
            message: String::from("Not found")
        }
    )
}

fn rocket() -> Rocket {
    rocket::ignite()
        .mount("/", routes![index])
        .mount("/users", routes![
            list_users,
            create_user, 
            get_user,
            delete_user,
            put_user
        ])
        .catch(catchers![not_found])
        // we want Rocket to keep track of this connection pool
        .manage(db_init_pool())
}

fn main() {
    rocket().launch();
}
