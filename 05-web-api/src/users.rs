use rocket_contrib::Json;

use diesel;
use diesel::prelude::*;
use db::DbConn;
use common::*;
use models::*;
use schema::users;
use schema::users::dsl::*;
use rocket::response::Failure;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserResponse {
    pub id: i32,
    pub email: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UserRequest {
    pub email: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password: Option<String>
}

impl From<User> for UserResponse {
    fn from(u: User) -> Self {
        UserResponse {
            id: u.id,
            email: u.email,
            first_name: u.first_name,
            last_name: u.last_name
        }
    }
}

impl From<UserRequest> for InsertableUser {
    fn from(u: UserRequest) -> Self {
        InsertableUser {
            email: u.email,
            first_name: u.first_name,
            last_name: u.last_name,
            password_hash: u.password
        }
    }
}

impl User {
    fn from_request(u: UserRequest, user_id: i32) -> Self {
        User {
            id: user_id,
            email: u.email,
            first_name: u.first_name,
            last_name: u.last_name,
            password_hash: u.password
        }
    }
}

#[get("/", format="application/json")]
pub fn list_users(conn: DbConn) -> Result<Json<Vec<UserResponse>>, Failure> {
    users::table.load::<User>(&*conn)
        .map(|v| Json(v.into_iter().map(|u| UserResponse::from(u)).collect()))
        .map_err(|err| error_status(err))
}

#[get("/<user_id>", format = "application/json")]
pub fn get_user(user_id: i32, conn: DbConn) -> Result<Json<UserResponse>, Failure> {
    users.find(user_id).get_result::<User>(&*conn)
        .map(|user| Json(UserResponse::from(user)))
        .map_err(|err| error_status(err))
}

#[post("/", format = "application/json", data = "<user>")]
pub fn create_user(user: Json<UserRequest>, conn: DbConn) -> Result<Json<UserResponse>, Failure> {
    let user_request = user.into_inner();
    println!("Got user creation request: {:?}", user_request);
    diesel::insert_into(users)
        .values(&InsertableUser::from(user_request))
        .get_result::<User>(&*conn)
        .map(|result| Json(UserResponse::from(result)))
        .map_err(|err| error_status(err))
}

#[put("/<user_id>", format = "application/json", data = "<user>")]
pub fn put_user(user_id: i32, user: Json<UserRequest>, conn: DbConn) -> Result<Json<UserResponse>, Failure> {
    diesel::update(users::table.find(user_id))
        .set(&User::from_request(user.into_inner(), user_id))
        .get_result::<User>(&*conn)
        .map(|u| Json(UserResponse::from(u)))
        .map_err(|e| error_status(e))
}

#[delete("/<user_id>")]
pub fn delete_user(user_id: i32, conn: DbConn) -> Result<Json<Response>, Failure> {
    match users.find(user_id).get_result::<User>(&*conn) {
        Ok(_)  => diesel::delete(users::table.find(user_id))
            .execute(&*conn)
            .map(|_| Json(Response { message: "Deleted".to_string() }))
            .map_err(|e| error_status(e)),
        Err(e) => Err(error_status(e))
    }
}
