# Intro to Rust

Included here in this repo are a bunch of sample applications written in Rust to
demonstrate different aspects of the Rust programming language.

* [01-hello-world](./01-hello-world/) - Rust's simple "Hello World" example.
* [02-guessing-game](./02-guessing-game/) - The guessing game example from the
  Rust Book tutorial.
* [03-cli](./03-cli/) - How to use the [clap](https://docs.rs/clap/2.32.0/clap/)
  library to parse command line arguments.
* [04-multithreading](./04-multithreading/) - A simple map/reduce sum operation
  using multithreading in Rust.
* [05-web-api](./05-web-api/) - A web API that currently just allows you to
  perform CRUD operations on `User` objects, backed by a PostgreSQL database.
  Uses [Rocket](https://rocket.rs/) and [Diesel](http://diesel.rs/), and relies
  on having the Rust Nightly toolchain installed.

