// we only tell the compiler about external crates in our main file
extern crate rand;

// these are the namespaces/modules we want to actually use
use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    // any callable with an exclamation mark (!) at the end of its name
    // is actually a macro
    println!("Guess the number!");

    // generate our secret number between 1 and 100 (inclusive)
    let secret_number = rand::thread_rng().gen_range(1, 101);

    // infinite loop
    loop {
        println!("Please input your guess.");

        // mutable variables have to be explicitly declared as such
        let mut guess = String::new();

        // the .expect() function will cause a panic and will terminate the
        // program if an error occurs while trying to read from stdin
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        // the compiler knows that we want to parse the string to a u32 because
        // it infers this from the type of the variable we're trying to assign it to
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_)  => continue,
        };

        // how to simply print variables' values
        println!("You guessed: {}", guess);

        // the cmp() function is provided by importing std::cmp::Ordering
        match guess.cmp(&secret_number) {
            Ordering::Less    => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal   => {
                println!("Correct! You win!");
                break;
            }
        }
    }
}
