# Guessing Game in Rust

This is pretty much exactly the code from the Rust Book's [Guessing
Game](https://doc.rust-lang.org/book/2018-edition/ch02-00-guessing-game-tutorial.html)
tutorial.

