extern crate clap;

use clap::{App, Arg, SubCommand};

fn main() {
    let matches = App::new("My super cool CLI")
        .version("0.1.0")
        .author("Thane Thomson <connect@thanethomson.com>")
        .about("Does awesome things")
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .multiple(true)
                .help("Sets output verbosity level")
        )
        .arg(
            Arg::with_name("input_file")
                .takes_value(true)
                .required(true)
                .help("The input file to use to generate awesomeness")
        )
        .subcommand(
            SubCommand::with_name("get")
                .about("Get a value from the input file")
                .arg(
                    Arg::with_name("delete")
                        .short("d")
                        .long("delete")
                        .help("Deletes the value after getting it")
                )
                .arg(
                    Arg::with_name("key")
                        .takes_value(true)
                        .required(true)
                        .help("The key for which to retrieve a value")
                )
        )
        .subcommand(
            SubCommand::with_name("put")
                .about("Put a value into the input file")
                .arg(
                    Arg::with_name("get")
                        .short("g")
                        .long("get")
                        .help("Retrieve and show the current value before putting the new value")
                )
                .arg(
                    Arg::with_name("key")
                        .takes_value(true)
                        .required(true)
                        .help("The key for which to put a value")
                )
                .arg(
                    Arg::with_name("value")
                        .takes_value(true)
                        .required(true)
                        .help("The value to put into the file")
                )
        )
        .get_matches();
    
    let verbosity_level = matches.occurrences_of("verbose");
    println!("Verbosity level: {}", verbosity_level);

    let input_file = matches.value_of("input_file").unwrap();
    println!("Input file name: {}", input_file);

    match matches.subcommand_name() {
        Some("get") => {
            if let Some(get_matches) = matches.subcommand_matches("get") {
                let key = get_matches.value_of("key").unwrap();
                println!("GET command invoked for key: {}", key);
            }
        },
        Some("put") => {
            if let Some(put_matches) = matches.subcommand_matches("put") {
                let key = put_matches.value_of("key").unwrap();
                let value = put_matches.value_of("value").unwrap();
                println!("PUT command invoked: {} => {}", key, value);
            }
        },
        Some(_)     => println!("Unknown command"),
        None        => println!("Missing command")
    }
}
